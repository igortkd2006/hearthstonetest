package br.com.hearthstonetest.data.repositories

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import br.com.hearthstonetest.data.models.ApiResponse
import br.com.hearthstonetest.data.services.HearthStoneService
import kotlinx.coroutines.*
import retrofit2.Response
import timber.log.Timber

abstract class BaseRepository<T>(@PublishedApi internal val service: HearthStoneService) {


    inline fun <reified T : Any> fetchListData(crossinline call: (HearthStoneService) -> Deferred<Response<ArrayList<T>>>): LiveData<ApiResponse<ArrayList<T>>> {
        val result = MutableLiveData<ApiResponse<ArrayList<T>>>()
        CoroutineScope(Dispatchers.IO).launch {
            val request = call(service)
            withContext(Dispatchers.Main) {
                try {
                    val response = request.await()
                    if (response.isSuccessful) {
                        result.value = ApiResponse<ArrayList<T>>(response.body(), response.code(), null)
                    } else {
                        Timber.d("Error occurred with code ${response.code()}")
                        result.value = ApiResponse<ArrayList<T>>(null, response.code(), "undefined error")
                    }
                } catch (e: Exception) {
                    result.value = ApiResponse<ArrayList<T>>(null, 500, e.message)
                }

            }
        }
        return result
    }

    inline fun <reified T : Any> fetchOneData(crossinline call: (HearthStoneService) -> Deferred<Response<T>>): LiveData<ApiResponse<T>> {
        val result = MutableLiveData<ApiResponse<T>>()
        CoroutineScope(Dispatchers.IO).launch {
            val request = call(service)
            withContext(Dispatchers.Main) {
                try {
                    val response = request.await()
                    if (response.isSuccessful) {
                        result.value = ApiResponse<T>(response.body(), response.code(), null)
                    } else {
                        Timber.d("Error occurred with code ${response.code()}")
                        result.value = ApiResponse<T>(null, response.code(), "undefined error")

                    }
                } catch (e: Exception) {
                    Timber.d("Error: ${e.message}")
                    result.value = ApiResponse<T>(null, 500, e.message)
                }
            }
        }
        return result
    }
}