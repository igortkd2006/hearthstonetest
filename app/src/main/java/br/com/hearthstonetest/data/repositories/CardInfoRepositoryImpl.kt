package br.com.hearthstonetest.data.repositories

import androidx.lifecycle.LiveData
import br.com.hearthstonetest.data.models.ApiResponse
import br.com.hearthstonetest.data.models.CardInfo
import br.com.hearthstonetest.data.services.HearthStoneService

open class CardInfoRepositoryImpl (service: HearthStoneService) : BaseRepository<ApiResponse<CardInfo>>(service), ICardInfoRepository {
    override fun getCardInfo(): LiveData<ApiResponse<CardInfo>> {
        return fetchOneData { service.getCardInfoAsync()}
    }

}