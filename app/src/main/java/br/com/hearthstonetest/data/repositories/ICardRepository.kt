package br.com.hearthstonetest.data.repositories

import androidx.lifecycle.LiveData
import br.com.hearthstonetest.data.CardCollectionEnum
import br.com.hearthstonetest.data.models.ApiResponse
import br.com.hearthstonetest.data.models.Card

interface ICardRepository {
    fun getCardsByCollection(value: String, cardCollectionEnum: CardCollectionEnum): LiveData<ApiResponse<ArrayList<Card>>>
}