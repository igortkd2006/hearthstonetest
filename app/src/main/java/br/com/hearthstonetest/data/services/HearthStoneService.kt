package br.com.hearthstonetest.data.services

import br.com.hearthstonetest.data.models.Card
import br.com.hearthstonetest.data.models.CardInfo
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path

interface HearthStoneService {


    @GET("info")
    @Headers("x-rapidapi-host: omgvamp-hearthstone-v1.p.rapidapi.com","x-rapidapi-key: 6c6834019cmsh63abecab7917cbcp18811cjsncd1398242f3a")
    fun getCardInfoAsync(): Deferred<Response<CardInfo>>

    @GET("/cards/classes/{class}")
    @Headers("x-rapidapi-host: omgvamp-hearthstone-v1.p.rapidapi.com","x-rapidapi-key: 6c6834019cmsh63abecab7917cbcp18811cjsncd1398242f3a")
    fun getCardListByClassesAsync(@Path("class")clazz: String): Deferred<Response<ArrayList<Card>>>

    @GET("/cards/types/{type}")
    @Headers("x-rapidapi-host: omgvamp-hearthstone-v1.p.rapidapi.com","x-rapidapi-key: 6c6834019cmsh63abecab7917cbcp18811cjsncd1398242f3a")
    fun getCardListByTypesAsync(@Path("type")type: String): Deferred<Response<ArrayList<Card>>>

    @GET("/cards/races/{race}")
    @Headers("x-rapidapi-host: omgvamp-hearthstone-v1.p.rapidapi.com","x-rapidapi-key: 6c6834019cmsh63abecab7917cbcp18811cjsncd1398242f3a")
    fun getCardListByRacesAsync(@Path("race")race: String): Deferred<Response<ArrayList<Card>>>



    companion object {
        fun create(): HearthStoneService {
            return Retrofit.Builder()
                .baseUrl("https://omgvamp-hearthstone-v1.p.rapidapi.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()
                .create(HearthStoneService::class.java)
        }
    }
}