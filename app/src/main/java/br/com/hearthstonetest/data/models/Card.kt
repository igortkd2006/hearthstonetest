package br.com.hearthstonetest.data.models


data class Card(
    val artist: String,
    val attack: Int,
    val cardId: String,
    val cardSet: String,
    val collectible: Boolean,
    val cost: Int,
    val elite: Boolean,
    val faction: String,
    val flavor: String,
    val health: Int,
    var img: String,
    val imgGold: String,
    val locale: String,
    val name: String,
    val race: String,
    val rarity: String,
    val text: String,
    val type: String
)