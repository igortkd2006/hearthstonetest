package br.com.hearthstonetest.data

enum class CardCollectionEnum {
    CLAZZ, TYPES , RACES
}