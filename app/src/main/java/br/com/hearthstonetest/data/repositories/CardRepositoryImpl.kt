package br.com.hearthstonetest.data.repositories

import androidx.lifecycle.LiveData
import br.com.hearthstonetest.data.CardCollectionEnum
import br.com.hearthstonetest.data.models.ApiResponse
import br.com.hearthstonetest.data.models.Card
import br.com.hearthstonetest.data.services.HearthStoneService

open class CardRepositoryImpl (service: HearthStoneService) : BaseRepository<Card>(service), ICardRepository {
    override fun getCardsByCollection(value: String, cardCollectionEnum: CardCollectionEnum): LiveData<ApiResponse<ArrayList<Card>>> {
        return fetchListData {
            when(cardCollectionEnum){
                CardCollectionEnum.CLAZZ -> service.getCardListByClassesAsync(value)
                CardCollectionEnum.TYPES -> service.getCardListByTypesAsync(value)
                CardCollectionEnum.RACES -> service.getCardListByRacesAsync(value)
            }
        }
    }


}