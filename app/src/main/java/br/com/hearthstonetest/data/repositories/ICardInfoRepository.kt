package br.com.hearthstonetest.data.repositories

import androidx.lifecycle.LiveData
import br.com.hearthstonetest.data.models.ApiResponse
import br.com.hearthstonetest.data.models.CardInfo

interface ICardInfoRepository {
    fun getCardInfo(): LiveData<ApiResponse<CardInfo>>
}