package br.com.hearthstonetest.domain.impl

import androidx.lifecycle.LiveData
import br.com.hearthstonetest.data.models.ApiResponse
import br.com.hearthstonetest.data.models.CardInfo
import br.com.hearthstonetest.data.repositories.CardInfoRepositoryImpl
import br.com.hearthstonetest.data.repositories.ICardInfoRepository
import br.com.hearthstonetest.domain.ICardInfoUseCase

class CardInfoUseCaseImpl (private val repository: CardInfoRepositoryImpl) :
    ICardInfoUseCase {
    override fun getLiveData(): LiveData<ApiResponse<CardInfo>> {
        return repository.getCardInfo()
    }
}