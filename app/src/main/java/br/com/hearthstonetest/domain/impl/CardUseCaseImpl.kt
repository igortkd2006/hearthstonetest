package br.com.hearthstonetest.domain.impl

import androidx.lifecycle.LiveData
import br.com.hearthstonetest.data.CardCollectionEnum
import br.com.hearthstonetest.data.models.ApiResponse
import br.com.hearthstonetest.data.models.Card
import br.com.hearthstonetest.data.repositories.CardRepositoryImpl
import br.com.hearthstonetest.domain.IUseCase

class CardUseCaseImpl (private val repository: CardRepositoryImpl) :
    IUseCase<ArrayList<Card>> {
    fun geLiveData(value: String, cardCollectionEnum: CardCollectionEnum): LiveData<ApiResponse<ArrayList<Card>>> {
        return repository.getCardsByCollection(value, cardCollectionEnum)
    }
}