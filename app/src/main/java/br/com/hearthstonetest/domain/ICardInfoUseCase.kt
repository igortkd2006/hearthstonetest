package br.com.hearthstonetest.domain

import androidx.lifecycle.LiveData
import br.com.hearthstonetest.data.models.ApiResponse
import br.com.hearthstonetest.data.models.CardInfo

interface ICardInfoUseCase : IUseCase<CardInfo> {
    fun getLiveData(): LiveData<ApiResponse<CardInfo>>
}