package br.com.hearthstonetest.domain.impl

import br.com.hearthstonetest.data.models.Card


class CardHelper() {
    fun checkUrls(cards: ArrayList<Card>): ArrayList<Card> {
        val resp = ArrayList<Card>()
        cards.forEach() {
            if (it.img != null && !it.img.contains("wow.zamimg.com")) {
                it.img = it.img.replace("http", "https")
                    resp.add(it);
            }
        }
        return resp
    }



}