package br.com.hearthstonetest.di

import android.app.Application
import br.com.hearthstonetest.data.repositories.CardInfoRepositoryImpl
import br.com.hearthstonetest.data.repositories.CardRepositoryImpl
import br.com.hearthstonetest.domain.impl.CardHelper
import br.com.hearthstonetest.data.services.HearthStoneService
import br.com.hearthstonetest.domain.impl.CardInfoUseCaseImpl
import br.com.hearthstonetest.domain.impl.CardUseCaseImpl
import br.com.hearthstonetest.presentation.viewmodel.CardInfoViewModel
import br.com.hearthstonetest.presentation.viewmodel.CardViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module

class HearthStoneApplication: Application(){
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@HearthStoneApplication)
            modules(listOf(
                repositoryModule,
                helperModule,
                networkModule,
                viewModelModule,
                useCaseModule
            ))
        }
    }
}
val networkModule = module {
    single { HearthStoneService.create() }
}
val useCaseModule = module {
    single { CardInfoUseCaseImpl(get()) }
    single { CardUseCaseImpl(get()) }
}

val repositoryModule = module {
    single { CardInfoRepositoryImpl(get()) }
    single { CardRepositoryImpl(get()) }

}

val viewModelModule = module {
    viewModel { CardInfoViewModel(get()) }
    viewModel { CardViewModel(get()) }
}

val helperModule = module {
    single { CardHelper() }
}

