package br.com.hearthstonetest.presentation.view

import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.LayerDrawable
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import br.com.hearthstonetest.R
import kotlinx.android.synthetic.main.item_cardinfo_layout.view.*
import kotlin.random.Random


class CardInfoViewHolder(private val view: View, private var listener: GenericAdapter.ClickItemListener,  private val enum: CardFunctionsEnum) :
    RecyclerView.ViewHolder(view), GenericAdapter.Binder<String> {

    override fun bind(data: String, position: Int) {
        view.main_item_name.text = data
        val color = Color.argb(255, Random.nextInt(255), Random.nextInt(255), Random.nextInt(255))
        val bgDrawable = view.getBackground() as LayerDrawable
        val gradDrawable = bgDrawable.findDrawableByLayerId(R.id.background_retancle_shape) as GradientDrawable
        gradDrawable.setColor(color);
        view.main_item_body.setOnClickListener(View.OnClickListener {listener.itemClickListener(data,enum)})

    }

}