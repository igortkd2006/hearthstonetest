package br.com.hearthstonetest.presentation.view

import android.graphics.Color
import android.os.StrictMode
import android.view.View
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import br.com.hearthstonetest.R
import br.com.hearthstonetest.data.models.Card
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import com.wang.avi.AVLoadingIndicatorView
import kotlinx.android.synthetic.main.cards_layout.*
import kotlinx.android.synthetic.main.item_card_layout.view.*
import kotlin.random.Random

class CardViewHolder(private val view: View, private val listener: CardViewHolderListener) :
    RecyclerView.ViewHolder(view),GenericAdapter.Binder<Card> {

    override fun bind(data: Card, position: Int) {
        startProgressAnim(itemView.progress_bar_card_holder, itemView.iv_card_cards)
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        try {
            Picasso.get().load(data.img)
                .into(itemView.iv_card_cards, object : Callback {
                    override fun onSuccess() {
                        stopProgressAnim(itemView.progress_bar_card_holder, itemView.iv_card_cards)
                    }
                    override fun onError(e: java.lang.Exception?) {
                        listener.errorOnImageLoder(position)
                    }
                })
        } catch (e: Exception) {
            println("error loading image: ${data.img}")
        }
    }

    open fun startProgressAnim(loader: AVLoadingIndicatorView, parent: View) {
        loader.setIndicatorColor(Color.argb(255, Random.nextInt(255), Random.nextInt(255), Random.nextInt(255)))
        parent.visibility = View.INVISIBLE
        if (!loader.isVisible) {
            loader.smoothToShow();
        }
    }

    open fun stopProgressAnim(loader: AVLoadingIndicatorView, parent: View) {
        if (loader.isVisible) {
            loader.smoothToHide();
        }
        parent.visibility = View.VISIBLE
    }



}

interface CardViewHolderListener {
    fun errorOnImageLoder(position: Int)
}
