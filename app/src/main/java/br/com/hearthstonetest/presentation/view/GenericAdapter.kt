package br.com.hearthstonetest.presentation.view
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.hearthstonetest.R
import br.com.hearthstonetest.presentation.view.CardFunctionsEnum.*

class GenericAdapter<T>(private val context: Context, private val items: MutableList<T>, private val enum: CardFunctionsEnum, private val listener: ClickItemListener)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>(), CardViewHolderListener {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (enum){
            INFO_CLAZZ -> CardInfoViewHolder(LayoutInflater.from(context).inflate(R.layout.item_cardinfo_layout, parent, false), listener, enum)
            INFO_RACES -> CardInfoViewHolder(LayoutInflater.from(context).inflate(R.layout.item_cardinfo_layout, parent, false), listener, enum)
            INFO_TYPES -> CardInfoViewHolder(LayoutInflater.from(context).inflate(R.layout.item_cardinfo_layout, parent, false), listener, enum)
            LIST-> CardViewHolder(LayoutInflater.from(context).inflate(R.layout.item_card_layout, parent, false),this)

        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as Binder<T>).bind(items[position], position)
    }

    override fun getItemCount(): Int = items.size

    internal interface Binder<T> {
        fun bind(data: T, position: Int)
    }

    override fun errorOnImageLoder(position: Int) {
        items.removeAt(position)
        notifyItemRemoved(position)
    }
    fun addItem(item : T){
        items.add(item)
        notifyItemInserted(items.size)
    }



    interface ClickItemListener{
        fun itemClickListener(data: String, dataType: CardFunctionsEnum)
    }


}