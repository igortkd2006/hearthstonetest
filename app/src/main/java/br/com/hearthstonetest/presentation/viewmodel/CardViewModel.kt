package br.com.hearthstonetest.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import br.com.hearthstonetest.data.CardCollectionEnum
import br.com.hearthstonetest.data.models.ApiResponse
import br.com.hearthstonetest.data.models.Card
import br.com.hearthstonetest.domain.impl.CardUseCaseImpl

class CardViewModel(private val useCaseImpl: CardUseCaseImpl) : ViewModel() {
    private lateinit var cardInfo: LiveData<ApiResponse<ArrayList<Card>>>
    fun getData(value: String, enum : CardCollectionEnum): LiveData<ApiResponse<ArrayList<Card>>> {
        cardInfo = useCaseImpl.geLiveData(value, enum)
        return cardInfo;
    }


}