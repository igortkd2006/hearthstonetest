package br.com.hearthstonetest.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import br.com.hearthstonetest.data.models.ApiResponse
import br.com.hearthstonetest.data.models.CardInfo
import br.com.hearthstonetest.domain.impl.CardInfoUseCaseImpl

class CardInfoViewModel(private val useCaseImpl: CardInfoUseCaseImpl) : ViewModel() {
    private lateinit var cardInfo: LiveData<ApiResponse<CardInfo>>

    fun getData(): LiveData<ApiResponse<CardInfo>> {
        cardInfo = useCaseImpl.getLiveData()
        return cardInfo;
    }
}