package br.com.hearthstonetest.presentation.view.activities

import android.graphics.Color
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.wang.avi.AVLoadingIndicatorView
import kotlin.random.Random

abstract class BaseActivity : AppCompatActivity() {
    open fun startProgressAnim(loader: AVLoadingIndicatorView, parent: View) {
        loader.setIndicatorColor(Color.argb(255, Random.nextInt(255), Random.nextInt(255), Random.nextInt(255)))
        parent.visibility = View.GONE
        if (!loader.isVisible) {
            loader.smoothToShow();
        }
    }

    open fun stopProgressAnim(loader: AVLoadingIndicatorView, parent: View) {
        if (loader.isVisible) {
            loader.smoothToHide();
        }
        parent.visibility = View.VISIBLE
    }

}