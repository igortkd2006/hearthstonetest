package br.com.hearthstonetest.presentation.view.activities

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import br.com.hearthstonetest.R
import br.com.hearthstonetest.data.models.CardInfo
import br.com.hearthstonetest.presentation.view.CardFunctionsEnum
import br.com.hearthstonetest.presentation.view.GenericAdapter
import br.com.hearthstonetest.presentation.viewmodel.CardInfoViewModel
import kotlinx.android.synthetic.main.main_layout.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class MainActivity : BaseActivity(), GenericAdapter.ClickItemListener {


    private val cardInfoVm: CardInfoViewModel by viewModel()
    private lateinit var sharedPref: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_layout)
        startProgressAnim(progress_bar_main, container_data_main)
        Timber.plant(Timber.DebugTree())
        setListeners()
        sharedPref = getPreferences(Context.MODE_PRIVATE)
        rv_classes_main?.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        rv_types_main?.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        rv_races_main?.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        my_swipeRefresh_Layout.setOnRefreshListener {
            setListeners()
        }
    }

    override fun onBackPressed() {


    }

    private fun setListeners() {
        downloadData(cardInfoVm)
    }

    private fun downloadData(viewModel: CardInfoViewModel) {
        val liveData = viewModel.getData()
        liveData.observe(this, Observer { data ->
            if (data != null && liveData.value!!.code == 200) {
                data.entity?.let { populateAdapters(it) }
                container_body_main.visibility = View.VISIBLE
            } else {
                Toast.makeText(this, "A error getting data happened, please check your connection", Toast.LENGTH_LONG)
                    .show()
                container_body_main.visibility = View.GONE
            }
            my_swipeRefresh_Layout.isRefreshing = false
            stopProgressAnim(progress_bar_main, container_data_main)
        })

    }

    private fun populateAdapters(data: CardInfo) {
        rv_classes_main.adapter =
            GenericAdapter(this@MainActivity, data.classes as ArrayList<String>, CardFunctionsEnum.INFO_CLAZZ, this)
        rv_types_main.adapter = GenericAdapter(
            this@MainActivity,
            data.types as ArrayList<String>, CardFunctionsEnum.INFO_TYPES, this
        )
        rv_races_main.adapter = GenericAdapter(
            this@MainActivity,
            data.races as ArrayList<String>, CardFunctionsEnum.INFO_RACES, this
        )
    }

    override fun itemClickListener(data: String, dataType: CardFunctionsEnum) {
        moveToListCards(data, dataType)
    }

    private fun moveToListCards(value: String, enum: CardFunctionsEnum) {
        val nextScreenIntent = Intent(this, CardsActivity::class.java).apply {
            putExtra("filter", value)
            putExtra("enum", enum)
        }
        startActivity(nextScreenIntent)

    }


}