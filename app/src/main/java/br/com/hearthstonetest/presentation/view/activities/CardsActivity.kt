package br.com.hearthstonetest.presentation.view.activities

import android.content.Context
import android.content.SharedPreferences
import android.graphics.Rect
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ItemDecoration
import br.com.hearthstonetest.R
import br.com.hearthstonetest.data.CardCollectionEnum
import br.com.hearthstonetest.data.models.Card
import br.com.hearthstonetest.domain.impl.CardHelper
import br.com.hearthstonetest.presentation.view.CardFunctionsEnum
import br.com.hearthstonetest.presentation.view.GenericAdapter
import br.com.hearthstonetest.presentation.viewmodel.CardViewModel
import kotlinx.android.synthetic.main.cards_layout.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import java.net.HttpURLConnection
import java.net.URL


class CardsActivity : BaseActivity(), GenericAdapter.ClickItemListener {


    private val cardVm: CardViewModel by viewModel()
    private lateinit var sharedPref: SharedPreferences
    private var filter: String = ""
    private lateinit var enum: CardFunctionsEnum


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.cards_layout)
        startProgressAnim(progress_bar_cards, container_data_cards)
        Timber.plant(Timber.DebugTree())
        sharedPref = getPreferences(Context.MODE_PRIVATE)
        rv_cards?.layoutManager = GridLayoutManager(this, 2)
        enum = (intent.getSerializableExtra("enum") as CardFunctionsEnum?)!!
        filter = intent.getStringExtra("filter")!!
        tv_title_cards.text = enum.functionName
        setListeners()
        back_main_cards.setOnClickListener { onBackPressed() }
        val spacingInPixels = resources.getDimensionPixelSize(R.dimen.item_margin_cards)
        rv_cards.addItemDecoration(SpacesItemDecoration(spacingInPixels))
//                s, ver crash do backpressed
    }

    class SpacesItemDecoration(private val space: Int) : ItemDecoration() {
        override fun getItemOffsets(outRect: Rect, itemPosition: Int, parent: RecyclerView) {
            outRect.left = space;
            outRect.right = space;
        }
    }

    private fun setListeners() {
        downloadData(cardVm)
    }

    private fun downloadData(viewModel: CardViewModel) {
        val liveData = when (enum) {
            CardFunctionsEnum.INFO_RACES -> viewModel.getData(filter, CardCollectionEnum.RACES)
            CardFunctionsEnum.INFO_CLAZZ -> viewModel.getData(filter, CardCollectionEnum.CLAZZ)
            CardFunctionsEnum.INFO_TYPES -> viewModel.getData(filter, CardCollectionEnum.TYPES)
            CardFunctionsEnum.LIST -> TODO()
        }
        liveData.observe(this, Observer { data ->
            if (data != null && data.code == 200) {
                if (data.entity.isNullOrEmpty()) {
                    Toast.makeText(this, "There are no Cards with this filter", Toast.LENGTH_LONG).show()
                } else {
                    data.entity?.let { populateAdapters(it) }
                }
            } else {
                Toast.makeText(
                    this,
                    "A error getting data happened, please check your connection or/and try again later ",
                    Toast.LENGTH_LONG
                )
                    .show()
            }
            stopProgressAnim(progress_bar_cards, container_data_cards)
        })

    }

    private fun populateAdapters(data: ArrayList<Card>) {
        val resp = CardHelper().checkUrls(data)
        if (resp.isNotEmpty()) {
            asyncLoadItems(resp)
            rv_cards.adapter = GenericAdapter(this, ArrayList<Card>(), CardFunctionsEnum.LIST, this)
        } else {
            stopProgressAnim(progress_bar_cards, container_data_cards)
            Toast.makeText(this, "There are no Cards with this filter", Toast.LENGTH_LONG).show()
        }
    }

    private fun asyncLoadItems(resp: ArrayList<Card>) {
        doAsync {
            resp.forEach() { card ->
                if (getCheckImage(card.img)) {
                    uiThread {
                        val adapter = rv_cards.adapter as GenericAdapter<Card>
                        adapter.addItem(card)
                        if (progress_bar_cards.visibility == View.VISIBLE) {
                            stopProgressAnim(progress_bar_cards, container_data_cards)
                        }
                    }
                }
            }
        }
    }

    private fun getCheckImage(stringUrl: String): Boolean {
        println("checking url: $stringUrl")
        return try {
            val url = URL(stringUrl)
            val httpconn: HttpURLConnection = url.openConnection() as HttpURLConnection
            httpconn.responseCode == HttpURLConnection.HTTP_OK
        } catch (e: Exception) {
            false
        }
    }

    override fun itemClickListener(data: String, dataType: CardFunctionsEnum) {
        TODO("Not yet implemented")
    }
}



