package br.com.hearthstonetest.usecases

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import br.com.hearthstonetest.data.models.ApiResponse
import br.com.hearthstonetest.data.models.CardInfo
import br.com.hearthstonetest.data.repositories.CardInfoRepositoryImpl
import br.com.hearthstonetest.data.services.HearthStoneService
import br.com.hearthstonetest.domain.impl.CardInfoUseCaseImpl
import br.com.hearthstonetest.presentation.viewmodel.CardInfoViewModel
import org.junit.*
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class CardInfoTest  {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()
    @Mock
    private lateinit var observerCardInfo: Observer<ApiResponse<CardInfo>>

    private lateinit var viewModel: CardInfoViewModel

    @Test
    fun `when CardInfo getDatasuccess`() {
        //Arrange
        val list = arrayListOf<String>("Elfs","Goblins")
        val cardInfo = CardInfo(
            list,list,list,"patch 5",list,list,list,list,list,list
        )
        val responsedMock = ApiResponse<CardInfo>(cardInfo, 200, null);
        val data = MutableLiveData<ApiResponse<CardInfo>>()
        data.postValue(responsedMock)
        val repository = MockCardInfoRepository(data, HearthStoneService.create())
        val cardInfoUC = CardInfoUseCaseImpl(repository)
        val cardInfoVM = CardInfoViewModel(cardInfoUC)
        cardInfoVM.getData().observeForever(observerCardInfo)
        //Act
        cardInfoVM.getData()
        //Assert
        verify(observerCardInfo).onChanged(responsedMock)
        assert(cardInfoVM.getData().value!!.code == 200)
        assert(cardInfoUC.getLiveData().value!!.code == 200)
        assert(repository.getCardInfo().value!!.code == 200)
    }

    @Test
    fun `when CardInfo getData error`() {
        //Arrange
        val response = ApiResponse<CardInfo>(null, 500, "some error");
        val data = MutableLiveData<ApiResponse<CardInfo>>()
        data.postValue(response)
        val repository = MockCardInfoRepository(data, HearthStoneService.create())
        val cardInfoUC = CardInfoUseCaseImpl(repository)
        val cardInfoVM = CardInfoViewModel(cardInfoUC)
        cardInfoVM.getData().observeForever(observerCardInfo)
        //Act
        cardInfoVM.getData()
        //Assert
        verify(observerCardInfo).onChanged(response)
        assert(cardInfoVM.getData().value!!.code == 500)
        assert(cardInfoUC.getLiveData().value!!.code == 500)
        assert(repository.getCardInfo().value!!.code == 500)
    }


    class MockCardInfoRepository(private val cardInfoLiveData: LiveData<ApiResponse<CardInfo>>, service: HearthStoneService) :
        CardInfoRepositoryImpl(service) {
        override fun getCardInfo(): LiveData<ApiResponse<CardInfo>> {
            return cardInfoLiveData
        }

    }
}