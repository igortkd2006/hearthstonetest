package br.com.hearthstonetest.usecases

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import br.com.hearthstonetest.data.CardCollectionEnum
import br.com.hearthstonetest.data.models.ApiResponse
import br.com.hearthstonetest.data.models.Card
import br.com.hearthstonetest.data.models.CardInfo
import br.com.hearthstonetest.data.repositories.CardRepositoryImpl
import br.com.hearthstonetest.data.services.HearthStoneService
import br.com.hearthstonetest.domain.impl.CardInfoUseCaseImpl
import br.com.hearthstonetest.domain.impl.CardUseCaseImpl
import br.com.hearthstonetest.presentation.viewmodel.CardInfoViewModel
import br.com.hearthstonetest.presentation.viewmodel.CardViewModel
import org.junit.*
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class CardListTest  {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()
    @Mock
    private lateinit var observerCard: Observer<ApiResponse<ArrayList<Card>>>

    private lateinit var viewModel: CardInfoViewModel

    @Test
    fun `when CardList getData success`() {
        //Arrange
        val filter = "Druid"
        val card = Card(
            "Artists", 100, "CardId","Card Set", true,
            1, true, "Faction", "Flavor", 1, "IMG", "IMGGOLD",
            "Locale", "Name", "Druid", "Rarity", "Text", "Hero"
        )

        val cards = arrayListOf<Card>(card,card,card,card,card,card,card,card)
        val responsedMock = ApiResponse<ArrayList<Card>>(cards, 200, null);
        val data = MutableLiveData<ApiResponse<ArrayList<Card>>>()
        data.postValue(responsedMock)
        val repository = MockCardRepository(data, HearthStoneService.create())
        val cardInfoUC = CardUseCaseImpl(repository)
        val cardInfoVM = CardViewModel(cardInfoUC)
        cardInfoVM.getData(filter, CardCollectionEnum.TYPES).observeForever(observerCard)
        //Act
        cardInfoVM.getData(filter, CardCollectionEnum.TYPES)
        //Assert
        verify(observerCard).onChanged(responsedMock)
        assert(cardInfoVM.getData(filter, CardCollectionEnum.TYPES).value!!.code == 200)
        assert(cardInfoUC.geLiveData(filter, CardCollectionEnum.TYPES).value!!.code == 200)
        assert(repository.getCardsByCollection(filter, CardCollectionEnum.TYPES).value!!.code == 200)
    }



    @Test
    fun `when CardList getData error`() {
        //Arrange
        val filter = "Druid"
        val response = ApiResponse<ArrayList<Card>>(arrayListOf(), 500, "some error");
        val data = MutableLiveData<ApiResponse<ArrayList<Card>>>()
        data.postValue(response)
        val repository = MockCardRepository(data, HearthStoneService.create())
        val cardInfoUC = CardUseCaseImpl(repository)
        val cardInfoVM = CardViewModel(cardInfoUC)
        cardInfoVM.getData(filter, CardCollectionEnum.TYPES).observeForever(observerCard)
        //Act
        cardInfoVM.getData(filter, CardCollectionEnum.TYPES)
        //Assert
        verify(observerCard).onChanged(response)
        assert(cardInfoVM.getData(filter, CardCollectionEnum.TYPES).value!!.code == 500)
        assert(cardInfoUC.geLiveData(filter, CardCollectionEnum.TYPES).value!!.code == 500)
        assert(repository.getCardsByCollection(filter, CardCollectionEnum.TYPES).value!!.code == 500)
    }


    class MockCardRepository(private val cardInfoLiveData: LiveData<ApiResponse<ArrayList<Card>>>, service: HearthStoneService) :
        CardRepositoryImpl(service) {
        override fun getCardsByCollection(
            value: String,
            cardCollectionEnum: CardCollectionEnum
        ): LiveData<ApiResponse<ArrayList<Card>>> {
            return cardInfoLiveData
        }





    }
}